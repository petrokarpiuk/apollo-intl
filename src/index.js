import { IntlProvider, addLocaleData, defineMessages } from 'react-intl'

import ru from 'react-intl/locale-data/ru'
import en from 'react-intl/locale-data/en'

import localeData from '../locales.json';

// Get language with fallback to the browser language or en
export function getLanguage(ln) {
  const lnCode = ln || 'en';

  return formatLanguage(lnCode);
}

export function getMessages(ln) {
  const language = ln || getLanguage();
  const messageData = localeData[language];

  return defineMessages(messageData);
}

/*
  Returns ln code
*/
export function formatLanguage(lnCode) {
  return lnCode.toLowerCase().split(/[_-]+/)[0];
}

/*
  Returns supported locales
*/
export function getLocales() {
  return [...ru, ...en]
}