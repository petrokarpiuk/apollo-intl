import { expect } from 'chai';
import { getLanguage, formatLanguage, getMessages, getLocales } from '../src/index';

describe('Internationalization', function() {
  it('should return default language', () => {
    expect(getLanguage()).to.equal('en')
  })

  it('should return russian locale', () => {
    expect(getLanguage('ru')).to.equal('ru')
  })

  it('should return messages for first locale', () => {
    const locales = getLocales()
    const ln = getLanguage(locales[0].locale)

    expect(getMessages(ln)).to.not.empty;
  })

})