'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getLanguage = getLanguage;
exports.getMessages = getMessages;
exports.formatLanguage = formatLanguage;
exports.getLocales = getLocales;

var _reactIntl = require('react-intl');

var _ru = require('react-intl/locale-data/ru');

var _ru2 = _interopRequireDefault(_ru);

var _en = require('react-intl/locale-data/en');

var _en2 = _interopRequireDefault(_en);

var _locales = require('../locales.json');

var _locales2 = _interopRequireDefault(_locales);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

// Get language with fallback to the browser language or en
function getLanguage(ln) {
  var lnCode = ln || 'en';

  return formatLanguage(lnCode);
}

function getMessages(ln) {
  var language = ln || getLanguage();
  var messageData = _locales2.default[language];

  return (0, _reactIntl.defineMessages)(messageData);
}

/*
  Returns ln code
*/
function formatLanguage(lnCode) {
  return lnCode.toLowerCase().split(/[_-]+/)[0];
}

/*
  Returns supported locales
*/
function getLocales() {
  return [].concat(_toConsumableArray(_ru2.default), _toConsumableArray(_en2.default));
}